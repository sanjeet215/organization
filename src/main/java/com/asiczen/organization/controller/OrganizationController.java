package com.asiczen.organization.controller;

import com.asiczen.organization.dto.Response;
import com.asiczen.organization.request.OrganizationRequest;
import com.asiczen.organization.service.OrganizationService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/service")
@Slf4j
public class OrganizationController {

    @Autowired
    OrganizationService organizationService;


    @PostMapping("/organization")
    @ResponseStatus(HttpStatus.CREATED)
    public Response createOrUpdateOrganization(@Valid @RequestBody OrganizationRequest request) {

        log.trace("Create organization controller post request is invoked. --> {} ", request);
        return organizationService.createOrUpdateOrganization(request);
    }

    @GetMapping("/organization")
    @ResponseStatus(HttpStatus.OK)
    public Response getOrganizationList() {
        log.trace("Getting list of all organizations");
        return organizationService.getAllOrganizations();
    }

    @GetMapping("/organization/{orgid}")
    @ResponseStatus(HttpStatus.OK)
    public Response getOrganizationById(@Valid @PathVariable("orgid") String orgid) {
        return organizationService.getOrganizationById(orgid);
    }

    @GetMapping("/organization/count")
    @ResponseStatus(HttpStatus.OK)
    public Response getOrganizationCount() {
        return organizationService.getOrganizationCount();
    }
}
