package com.asiczen.organization.service;

import com.asiczen.organization.dto.Response;
import com.asiczen.organization.request.OrganizationRequest;
import org.springframework.stereotype.Service;

@Service
public interface OrganizationService {

    public Response getOrganizationCount();

    public Response createOrUpdateOrganization(OrganizationRequest request);

    public Response getAllOrganizations();

    public Response getOrganizationById(String id);
}
