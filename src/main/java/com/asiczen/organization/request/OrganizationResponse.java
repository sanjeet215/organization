package com.asiczen.organization.request;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrganizationResponse {

    private String orgRefName;
    private String orgName;
    private String description;
    private boolean status;
    private String orgId;
    private String imageUrl;
}
