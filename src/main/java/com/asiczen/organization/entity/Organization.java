package com.asiczen.organization.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "organization")
@Getter
@Setter
@RequiredArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Organization implements Serializable {

    private static final long serialVersionUID = 2782735998562964416L;

    @Id
    @Column(name = "org_id")
    private String orgId;

    private String orgRefName;

    private String orgName;

    private String description;

    private boolean status;

    private String imageUrl;

    private boolean markForDeletion;

    private LocalDateTime purgeDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Organization)) return false;
        Organization that = (Organization) o;
        return isStatus() == that.isStatus() && isMarkForDeletion() == that.isMarkForDeletion() && Objects.equals(getOrgId(), that.getOrgId()) && Objects.equals(getOrgRefName(), that.getOrgRefName()) && Objects.equals(getOrgName(), that.getOrgName()) && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getImageUrl(), that.getImageUrl()) && Objects.equals(getPurgeDate(), that.getPurgeDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrgId(), getOrgRefName(), getOrgName(), getDescription(), isStatus(), getImageUrl(), isMarkForDeletion(), getPurgeDate());
    }
}