package com.asiczen.organization.serviceimpl;

import com.asiczen.organization.common.MeteringMessages;
import com.asiczen.organization.dto.Response;
import com.asiczen.organization.entity.Organization;
import com.asiczen.organization.exception.ResourceNotFoundException;
import com.asiczen.organization.repositories.OrganizationRepository;
import com.asiczen.organization.request.OrganizationRequest;
import com.asiczen.organization.request.OrganizationResponse;
import com.asiczen.organization.service.OrganizationService;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    OrganizationRepository organizationRepository;

    @Override
    public Response getOrganizationCount() {
        return new Response(MeteringMessages.RECORDS_EXTRACTED_SUCCESSFULLY.toString(), organizationRepository.count());
    }

    @Override
    public Response createOrUpdateOrganization(OrganizationRequest request) {
        Organization organization = getOrganizationEntity(request);
        organizationRepository.save(organization);
        return new Response(MeteringMessages.RECORD_CREATED_SUCCESSFULLY.toString(), organization);
    }

    @Override
    public Response getAllOrganizations() {
        return new Response(MeteringMessages.RECORDS_EXTRACTED_SUCCESSFULLY.toString(), organizationRepository.findAll()
                .stream().map(this::getOrganizationResponse)
                .collect(Collectors.toList()));
    }

    @Override
    public Response getOrganizationById(String id) {
        Organization organization = organizationRepository.findByOrgId(id).
                orElseThrow(() -> new ResourceNotFoundException(MeteringMessages.RECORD_WITH_ID_DOES_NOT_EXIST.toString()));

        return new Response(MeteringMessages.RECORDS_EXTRACTED_SUCCESSFULLY.toString(), organization);
    }


    private Organization getOrganizationEntity(OrganizationRequest request) {

        Organization organization = new Organization();

        if (StringUtils.isNotBlank(request.getOrgId())) {
            Optional<Organization> organizationOptional = organizationRepository.findByOrgId(request.getOrgId());
            if (organizationOptional.isPresent()) {
                organization = organizationOptional.get();
            }
        }

        if (!StringUtils.isNotBlank(request.getOrgId())) {
            organization.setOrgId(UUID.randomUUID().toString());
        } else {
            organization.setOrgId(request.getOrgId());
        }
        organization.setOrgRefName(request.getOrgRefName());
        organization.setOrgName(request.getOrgName());
        organization.setStatus(request.getStatus() == null);
        organization.setImageUrl(request.getImageUrl());
        organization.setDescription(request.getDescription());
        return organization;
    }

    private OrganizationResponse getOrganizationResponse(Organization organization) {
        OrganizationResponse organizationResponse = new OrganizationResponse();
        organizationResponse.setOrgRefName(organization.getOrgRefName());
        organizationResponse.setOrgName(organization.getOrgName());
        organizationResponse.setDescription(organization.getDescription());
        organizationResponse.setOrgId(organization.getOrgId());
        organizationResponse.setStatus(organizationResponse.isStatus());

        return organizationResponse;
    }
}
