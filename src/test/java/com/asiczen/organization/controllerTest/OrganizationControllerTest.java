package com.asiczen.organization.controllerTest;

import com.asiczen.organization.controller.OrganizationController;
import com.asiczen.organization.dto.Response;
import com.asiczen.organization.request.OrganizationRequest;
import com.asiczen.organization.utility.JsonUtil;
import java.io.IOException;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class OrganizationControllerTest {

    @Autowired
    OrganizationController organizationController;

    @Test
    void createOrganizationTest() throws IOException, JSONException {

        // Create API testcases
        OrganizationRequest organizationRequest = JsonUtil.readJsonFile("organization/organization.json", OrganizationRequest.class);
        Response organizationResponseExpected = JsonUtil.readJsonFile("organization/organizationResponse.json", Response.class);
        Response organizationResponseActual = organizationController.createOrUpdateOrganization(organizationRequest);
        JsonUtil.assetObject(organizationResponseActual, organizationResponseExpected);
//        Response organizationCountActual = organizationController.getOrganizationCount();
//        Response organizationCountExpected = JsonUtil.readJsonFile("organization/organizationCountResponse.json", Response.class);
//        JsonUtil.assetObject(organizationCountActual, organizationCountExpected);

        // Update API testcases
        OrganizationRequest organizationUpdateRequest = JsonUtil.readJsonFile("organization/organizationUpdate.json", OrganizationRequest.class);
        organizationController.createOrUpdateOrganization(organizationUpdateRequest);
        Response organizationUpdateResponseExpected = JsonUtil.readJsonFile("organization/organizationUpdateResponse.json", Response.class);
        Response organizationUpdateResponseActual = organizationController.getOrganizationById(organizationRequest.getOrgId());
        JsonUtil.assetObject(organizationUpdateResponseActual, organizationUpdateResponseExpected);

    }

//    @Test
//    void createOrganizationValidationTest() throws IOException, JSONException {
//        OrganizationRequest organizationRequest = JsonUtil.readJsonFile("invalidOrganization/organization.json", OrganizationRequest.class);
//        Response organizationResponseActual = organizationController.createOrUpdateOrganization(organizationRequest);
//        Response organizationResponseExpected = JsonUtil.readJsonFile("organization/organizationResponse.json", Response.class);
//        JsonUtil.assetObject(organizationResponseActual,organizationResponseExpected);
//    }
}
