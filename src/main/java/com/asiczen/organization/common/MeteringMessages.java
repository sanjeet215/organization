package com.asiczen.organization.common;

public enum MeteringMessages {
    RECORD_WITH_ID_DOES_NOT_EXIST("Records with ID doesn't exist"),
    RECORDS_EXTRACTED_SUCCESSFULLY("Records extracted successfully"),
    RECORD_CREATED_SUCCESSFULLY("Records created successfully");

    private final String message;

    MeteringMessages(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
