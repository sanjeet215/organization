package com.asiczen.organization.utility;

import com.asiczen.organization.dto.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.core.io.ClassPathResource;

public class JsonUtil {

    private final static String resourcePath = "src/test/resources/";

    public static ObjectMapper objectMapper = new ObjectMapper();

    public static boolean fileResourceExists(String fileName) {
        return new ClassPathResource(fileName).exists() || new File(fileName).exists();
    }

    public static <T> T readJsonFile(String filepath, Class<T> eClass) throws IOException {
        String content = Files.readString(Path.of(resourcePath + filepath), StandardCharsets.US_ASCII);
        return objectMapper.readValue(content, eClass);
    }

    public static void assetObject(Response actual, Response expected) throws JsonProcessingException, JSONException {
        String actualString = objectMapper.writeValueAsString(actual);
        String expectedString = objectMapper.writeValueAsString(expected);
        System.out.println(actualString);
        System.out.println(expectedString);
        JSONAssert.assertEquals(expectedString, actualString, JSONCompareMode.LENIENT);
    }
}
